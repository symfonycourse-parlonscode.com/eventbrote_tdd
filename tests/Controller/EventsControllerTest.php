<?php

namespace App\Tests\Controller;

use App\Entity\Event;
use App\Tests\Framework\WebTestCase;
use DateTime;

class EventsControllerTest extends WebTestCase
{
    public function test_index_should_list_all_events()
    {
        $event1 = (new Event)
            ->setName('Symfony Conférence')
            ->setPrice(0)
            ->setLocation('Paris, FR')
            ->setDescription('the best Symfony conférence')
            ->setStartAt(new DateTime('+ 15 days'));

        $event2 = (new Event)
            ->setName('Laravel Conférence')
            ->setPrice(20)
            ->setLocation('Quebec, CA')
            ->setDescription('the best Laravel conférence')
            ->setStartAt(new DateTime('+ 25 days'));
            
        $event3 = (new Event)
            ->setName('Django Conférence')
            ->setPrice(12)
            ->setLocation('Lomé, TG')
            ->setDescription('the best Django conférence')
            ->setStartAt(new DateTime('+ 65 days'));;
        


        $this->em->persist($event1);
        $this->em->persist($event2);
        $this->em->persist($event3);
        $this->em->flush();

        $this->client->request('GET', '/events');
        
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', '3 Events');
        $this->assertSelectorTextContains('body', $event1->getName());
        $this->assertSelectorTextContains('body', $event1->getDescription());
        $this->assertSelectorTextContains('body', 'FREE!');
        $this->assertSelectorTextContains('body', $event1->getLocation());
        $this->assertSelectorTextContains('body', $event1->getStartAt()->format($this->getParameter('app.default_date_format')));
        $this->assertSelectorTextContains('body', $event2->getName());
        $this->assertSelectorTextContains('body', $event3->getName());
    }

    public function test_show_should_list_the_event_details ()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));
        $this->em->persist($event);
        $this->em->flush();

        $this->client->request('GET', '/events/' . $event->getId());

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', $event->getName());
        $this->assertSelectorTextContains('body', $event->getDescription());
        $this->assertSelectorTextContains('body', $event->getPrice());
        $this->assertSelectorTextContains('body', $event->getLocation());
        $this->assertSelectorTextContains('body', $event->getStartAt()->format($this->getParameter('app.default_date_format')));
    }

    public function test_navigation_from_show_to_index_page_should_work()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));
        $this->em->persist($event);
        $this->em->flush();

        $this->client->request('GET', '/events/' . $event->getId());
        $this->assertResponseIsSuccessful();
        $this->client->clicklink('All events');
        $this->assertRouteSame('events.index');

    }

    public function test_navigation_from_index_to_show_page_should_work()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));
        $this->em->persist($event);
        $this->em->flush();

        $this->client->request('GET', '/events');
        $this->client->clicklink($event->getName());
        $this->assertSelectorTextContains('body', $event->getDescription());
        $this->assertRouteSame('events.show');
        //$this->assertResponseIsSuccessful();)

    }

    public function test_the_event_price_should_be_displayed_if_the_price_is_neither_zero_or_null()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(25)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));
        $this->em->persist($event);
        $this->em->flush();

        $this->client->request('GET', '/events/' . $event->getId());
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', '$25');

    }

    public function test_free_should_be_displayed_if_the_price_is_zero_or_null()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));
        $this->em->persist($event);
        $this->em->flush();

        $this->client->request('GET', '/events/' . $event->getId());
        $this->assertResponseIsSuccessful();
       $this->assertSelectorTextContains('body', 'FREE!');
    }

    public function test_swhow_should_return_a_404_response_if_we_cant_find_a_event_with_the_specified_id()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));
        $this->em->persist($event);
        $this->em->flush();
        $this->client->request('GET', '/events/1');
        $this->assertResponseIsSuccessful();

        $this->client->request('GET', '/events/2000');
        $this->assertResponseStatusCodeSame(404);
    }
}