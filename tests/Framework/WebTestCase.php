<?php

namespace App\Tests\Framework;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as TestWebTestCase;

class WebTestCase extends TestWebTestCase
{
    protected $em;
    protected $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->em = static::$container->get('doctrine')->getManager();

        $schemaTool = new SchemaTool($this->em);
        $schemaTool->dropDatabase();

        $metadata = $this->em->getMetadataFactory()->getAllMetadata();
    
        $schemaTool->createSchema($metadata);
    }

    protected function getParameter($name)
    {
        return static::$container->getParameter($name);
    }

    protected function tearDown(): void
    {
        /* $this->em->rollback(); */
        parent::tearDown();
        $this->em->close();
        $this->em = null;
    }
}