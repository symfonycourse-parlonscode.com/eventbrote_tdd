<?php

namespace App\Tests\Entity;

use App\Entity\Event;
use DateTime;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase
{
    public function test_an_event_should_not_be_free_if_the_price_is_neither_zero_or_null()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(25)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));

        $this->assertFalse($event->isFree());
    }

    public function test_an_event_should_be_free_if_the_price_is_zero()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));

        $this->assertTrue($event->isFree());
    }

    public function test_an_event_should_be_free_if_the_price_is_null()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(null)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));

        $this->assertTrue($event->isFree());
    }
}
