<?php

namespace App\Tests\Twig;

use App\Entity\Event;
use App\Twig\AppExtension;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Matcher\UrlMatcher;

class AppExtensionTest extends TestCase
{
    public function testSomething()
    {
        $event = (new Event)
        ->setName('Symfony Conférence')
        ->setPrice(0)
        ->setLocation('Paris, FR')
        ->setDescription('the best Symfony conférence')
        ->setStartAt(new DateTime('+ 15 days'));

        $appExtension = new AppExtension;
        $appExtension->formatPrice($event);
        $this->assertTrue(true);

    }

    /**
     * test_pluralize_should_raise_an_exception_if_the_count_is_invalid
     * @dataProvider pluralizeProvider
     */
    public function test_pluralize_should_work($expected, $count, $singular, $plural)
    {
        $appExtension = new AppExtension;
        $this->assertSame($expected, $appExtension->pluralize($count, $singular, $plural) );
    }
    
    public function test_pluralize_should_raise_an_exception_if_the_count_is_invalid()
    {
        $this->expectException(\InvalidArgumentException::class);

        $appExtension = new AppExtension;
        $this->assertTrue('45 Events' === $appExtension->pluralize('WRONG_VALUE', 'Event', 'Events') );
    }

    public function pluralizeProvider()
    {
        return [
            ['0 Events', 0, 'Event', 'Events'],
            ['1 Event', 1, 'Event', 'Events'],
            ['45 Events', 45, 'Event', 'Events'],
            ['1000 Events', 1000, 'Event', 'Events']
         ];
    }


}
